import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        final String MALE = "male";
        final String FEMALE = "female";

        Scanner scanner = new Scanner(System.in);
        System.out.print("Provide pesel number: " );
        String pesel = scanner.nextLine();
        Pattern pattern = Pattern.compile("[0-9]{11}");
        Matcher matcher = pattern.matcher(pesel);
        if (!matcher.matches()) {
            System.out.println("Pesel format is not correct");
            return;
        }
        int checksum = 0;
        char[] array = pesel.toCharArray();
        checksum += 9 *  Integer.parseInt(String.valueOf(array[0]));
        checksum += 7 * Integer.parseInt(String.valueOf(array[1]));
        checksum += 3 * Integer.parseInt(String.valueOf(array[2]));
        checksum += 1 * Integer.parseInt(String.valueOf(array[3]));
        checksum += 9 * Integer.parseInt(String.valueOf(array[4]));
        checksum += 7 * Integer.parseInt(String.valueOf(array[5]));
        checksum += 3 * Integer.parseInt(String.valueOf(array[6]));
        checksum += 1 * Integer.parseInt(String.valueOf(array[7]));
        checksum += 9 * Integer.parseInt(String.valueOf(array[8]));
        checksum += 7 * Integer.parseInt(String.valueOf(array[9]));

        if (checksum % 10 != Integer.parseInt(String.valueOf(array[10]))) {
            System.out.println("Pesel is not correct");
            return;
        }
        String gender;
        if (array[9] % 2 == 0) {
            gender = FEMALE;
        } else {
            gender = MALE;
        }

        StringBuilder sb = new StringBuilder();

        sb.append(array[4]);
        sb.append(array[5]);
        sb.append("-");
        sb.append(array[2]);
        sb.append(array[3]);
        sb.append("-");
        sb.append("19");
        sb.append(array[0]);
        sb.append(array[1]);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        LocalDate localDate = LocalDate.parse(sb.toString(), formatter);
        System.out.println("Birth date: " + localDate.toString());
        System.out.println("Gender: " + gender);
    }
}
